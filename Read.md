New WebApp template
====

Overview

WebAppの初期環境構築テンプレート

## Description

WebAppの初期環境構築をテンプレート化したもの、
自身の開発初期コストをさげるために作成

## Requirement

Node.js v9.8.0~  gulp version 3.9.1~

## Usage

gulp serve

## Install

npm install 

## Licence

[MIT](https://gitlab.com/iwamotoW/new-webApp-template/blob/master/LICENSE)

## Author

[iwamotoW](https://github.com/iwamotoW)