const gulp = require('gulp')
const plumber = require('gulp-plumber')
const sass = require('gulp-sass')
const watch = require('gulp-watch')
const browserSync = require('browser-sync').create()
const cleanCSS = require('gulp-clean-css')
const replace = require('gulp-replace')

const src = {
  sass: './src/sass/**/*.scss',
  html: './src/html/**/*.html',
  js: './src/js/**/*.js',
}

const htmls = () => {
    return gulp.src(src.html)
      .pipe(plumber())
      .pipe(gulp.dest('./dist'))
  }

const styles = () => {
    return gulp.src(src.sass)
      .pipe(sass())
      .pipe(cleanCSS({
        level: 2
      }))
      .pipe(replace('@charset "UTF-8"', ''))
      .pipe(gulp.dest('./dist/assets/css'))
  }

const scripts = () => {
    return gulp.src('./src/js/**/*')
        .pipe(gulp.dest('./dist/assets/js'))
}

const watchs = () => {
    watch(src.sass,styles)
    watch(src.js,scripts)
    watch(src.html,htmls)
    watch('./dist/**', () => {
        browserSync.reload()
    })
}

const serve = gulp.parallel(watchs, () => {
    return browserSync.init({
      open: true,
      ghostMode: false,
      server: {
        baseDir: './dist'
      }
    })
})

const build = gulp.series(gulp.parallel(styles,scripts,htmls))

exports.watch = watchs
exports.serve = serve
exports.build = build
gulp.task('default', serve)
